package com.example.zerosession.MainScreen.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.window.SplashScreen
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.zerosession.RecyclerActivity.RecyclerActivity
import com.example.zerosession.SplashScreen.SplashActivity
import com.example.zerosession.databinding.FragmentSecondBinding

//Фрагмент с кнопкой
class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val root = binding.root

        //При нажатии на кнопку
        binding.button2.setOnClickListener {
            val intent = Intent(inflater.context, RecyclerActivity::class.java)
            startActivity(intent)
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}