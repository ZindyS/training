package com.example.zerosession.MainScreen.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

//Названия вкладок
private val tabTitles = arrayOf(
    "Tab1",
    "Tab2"
)

//Класс, открывающий вкладки
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    //Создаёт вкладку
    override fun getItem(position: Int): Fragment {
        if (position==0) { return FirstFragment() }
        else if (position==1) { return  SecondFragment() }
        return FirstFragment()
    }

    //Возвращает название вкладки
    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

    //Возвращает кол-во вкладок
    override fun getCount(): Int {
        return tabTitles.size
    }
}