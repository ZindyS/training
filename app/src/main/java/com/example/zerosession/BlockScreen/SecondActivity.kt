package com.example.zerosession.BlockScreen

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.zerosession.OnboardingScreen.OnBoardingScreen
import com.example.zerosession.R
import com.example.zerosession.common.Response
import com.example.zerosession.common.SomeAPI
import kotlinx.android.synthetic.main.activity_second.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SecondActivity : AppCompatActivity() {
    var position = 0
    var ssize = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val retrofit = Retrofit.Builder()
            .baseUrl("http://mskko2021.mad.hakta.pro/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val api = retrofit.create(SomeAPI::class.java)

        api.getFeel().enqueue(object: Callback<Response> {
            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                if (response.isSuccessful) {
                    ssize = response.body()!!.data.size
                    viewPager.adapter = RecyclerAdapter(response.body()!!.data)
                } else {
                    Toast.makeText(this@SecondActivity, "Ошибка ${response.message()}", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Response>, t: Throwable) {
                Toast.makeText(this@SecondActivity, "Ошибка ${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
        viewPager.onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(p0: View?, p1: Boolean) {
                Toast.makeText(this@SecondActivity, "${viewPager.currentItem}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun onNextClicked(v: View) {
        position = viewPager.currentItem
        if (position == ssize-1) {
            val intent = Intent(this, OnBoardingScreen::class.java)
            startActivity(intent)
        } else {
            position++
            viewPager.currentItem = position
            if (position==1) {
                first.setCardBackgroundColor(Color.WHITE)
                second.setCardBackgroundColor(Color.parseColor("#C4C4C4"))
            } else if (position==2) {
                second.setCardBackgroundColor(Color.WHITE)
                third.setCardBackgroundColor(Color.parseColor("#C4C4C4"))
            } else if (position==3) {
                third.setCardBackgroundColor(Color.WHITE)
                fourth.setCardBackgroundColor(Color.parseColor("#C4C4C4"))
            } else {
                fourth.setCardBackgroundColor(Color.WHITE)
                fifth.setCardBackgroundColor(Color.parseColor("#C4C4C4"))
            }
        }
    }

    fun onSkipClicked(v: View) {
        val intent = Intent(this, OnBoardingScreen::class.java)
        startActivity(intent)
    }
}