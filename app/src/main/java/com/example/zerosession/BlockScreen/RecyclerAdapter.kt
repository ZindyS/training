package com.example.zerosession.BlockScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zerosession.R
import com.example.zerosession.common.Data
import kotlinx.android.synthetic.main.activity_login.view.*
import kotlinx.android.synthetic.main.rec_item2.view.*

//Кастомный адаптер для прокручиваемого списка
class RecyclerAdapter(val list: List<Data>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //Кастомный класс с описанием элемента
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textV = itemView.textView
        val image = itemView.imageView4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.rec_item2, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder = holder as VH
        holder.textV.text = list[position].title
        Glide.with(holder.image)
            .load(list[position].image)
            .into(holder.image)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}