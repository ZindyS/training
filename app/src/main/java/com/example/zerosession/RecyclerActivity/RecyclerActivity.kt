package com.example.zerosession.RecyclerActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zerosession.R
import kotlinx.android.synthetic.main.activity_recycler.*

//Экран с прокручиваемым списком
class RecyclerActivity : AppCompatActivity() {
    val list = listOf("First", "Second", "3", "4", "5", "6", "7", "8")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        recyclerView.adapter = RecyclerAdapter(list)
    }
}