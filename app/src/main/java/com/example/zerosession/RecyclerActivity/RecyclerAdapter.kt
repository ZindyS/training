package com.example.zerosession.RecyclerActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zerosession.R
import kotlinx.android.synthetic.main.rec_item.view.*

//Кастомный адаптер для прокручиваемого списка
class RecyclerAdapter(val list: List<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //Кастомный класс с описанием элемента
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textV = itemView.textView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.rec_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder = holder as VH
        holder.textV.text = list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }
}