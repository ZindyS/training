package com.example.zerosession.SplashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.zerosession.BlockScreen.SecondActivity
import com.example.zerosession.OnboardingScreen.OnBoardingScreen
import com.example.zerosession.R
import com.example.zerosession.SignInScreen.LoginActivity

//Стортовый экран
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //Задержка
        Handler().postDelayed({
            //Переход на следующий экран
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }, 2000)
    }
}