package com.example.zerosession.OnboardingScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.zerosession.R
import com.example.zerosession.RegisterScreen.RegisterActivity
import com.example.zerosession.SignInScreen.LoginActivity

class OnBoardingScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
    }

    fun onClick (v: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    fun onRegisterClick (v: View) {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}