package com.example.zerosession.SignInScreen

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.zerosession.MainScreen.TabbedActivity
import com.example.zerosession.SplashScreen.SplashActivity
import com.example.zerosession.R
import com.example.zerosession.RegisterScreen.RegisterActivity
import com.example.zerosession.common.SomeAPI
import com.example.zerosession.common.User
import com.example.zerosession.common.UserResponse
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Экран с авторизацией
class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    //При нажатии на кнопку "войти"
    fun onLoginClicked(v: View) {
        //Проверка на пустоту
        if (editTextTextPassword.text.isNotEmpty() && editTextTextPersonName.text.isNotEmpty()) {
            val retrofit = Retrofit.Builder()
                .baseUrl("http://mskko2021.mad.hakta.pro/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val api = retrofit.create(SomeAPI::class.java)

            api.login(User(editTextTextPersonName.text.toString(), editTextTextPassword.text.toString())).enqueue(object : Callback<UserResponse> {
                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    if (response.isSuccessful) {
                        val intent = Intent(this@LoginActivity, TabbedActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this@LoginActivity, "Ошибка ${response.message()}", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    Toast.makeText(this@LoginActivity, "Ошибка ${t.message}", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            AlertDialog.Builder(this)
                .setMessage("Пустые поля")
                .setTitle("Ошибка")
                .setNegativeButton("Ok") { dialogInterface: DialogInterface, i: Int -> }
                .show()
        }
    }

    fun onRegisterClick (v: View) {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}