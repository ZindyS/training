package com.example.zerosession.common


import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

//Класс с запросами
interface SomeAPI {
    @GET("feelings")
    fun getFeel() : Call<Response>

    @POST("user/login")
    fun login(@Body user: User) : Call<UserResponse>
}