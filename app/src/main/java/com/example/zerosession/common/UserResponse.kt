package com.example.zerosession.common

data class UserResponse(
    val error: String,
    val success: String,
    val id: String,
    val email: String,
    val nickName: String,
    val avatar: String,
    val token: String
)
