package com.example.zerosession.common

import java.text.FieldPosition

data class Data(
    val id : String,
    val title : String,
    val position : String,
    val image : String
)
