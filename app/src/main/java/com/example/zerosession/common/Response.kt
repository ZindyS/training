package com.example.zerosession.common

data class Response(
    val success: Boolean,
    val data: List<Data>
)
