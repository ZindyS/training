package com.example.zerosession.common

data class User(
    val email: String,
    val password: String
)
